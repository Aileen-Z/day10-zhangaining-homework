import { configureStore } from '@reduxjs/toolkit';
import todoReducer from '../components/todoListSlice';

export default configureStore({
  reducer: {todo:todoListReducer},
});