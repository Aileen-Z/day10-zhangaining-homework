import {createSlice} from "@reduxjs/toolkit";

export const todoListSlice = createSlice({
    name: 'todolist',
    initialState: {
        todoList: []
    },
    reducers: {
        addTodo: (state, action) => {
            state.todoList = [...state.todoList, action.payload]
        },
        deleteTodo: (state, action) => {
            const { id } = action.payload
            state.todoList = state.todoList.filter(todo => todo.id !== id)
        },
        complete: (state, action) => {
            const { id,done } = action.payload
            state.todoList.filter(todo => todo.id === id).map(todo => {
                    !done && (todo.done = !todo.done)
            })
        }
    },
})

export default todoListSlice.reducer;
export const {addTodo,deleteTodo,complete} = todoListSlice.actions;

