import {TodoGroup} from "./TodoGroup";
import {TodoGenerator} from "./TodoGenerator";
import './TodoList.css'

export const TodoList = () => {
    const title = "Todo List"
    return (
        <div className='todo-list-parent'>
            <h1>{ title }</h1>
            <TodoGroup/>
            <TodoGenerator/>
        </div>
    )
}
export default TodoList;
