// import { useState } from 'react';
// import {useDispatch} from 'react-redux'
// import {addTodo} from './todoListSlice'
// const TodoGenerator = () => {
//     const [newTodo, setNewTodo] = useState({id :0, text:'', done:false});
//     const dispatch=useDispatch();
//     const handleSubmit = (e) => {
//       e.preventDefault();
//       if (newTodo.text===''||newTodo.text===null) {
//         console.log('addTodo======================',newTodo)
//         addTodo(newTodo);
//         setNewTodo('');
//       }
//     };
//     const handleAdd=()=>{
//       dispatch(addTodo(newTodo))
//       setNewTodo({})
//     }
  
//     return (
//       <form onSubmit={handleSubmit}>
//         <input
//           type="text"
//           value={newTodo.text}
//           onChange={(e) => handleAdd(e.target.value)}
//         />
//         <button type="submit">Add</button>
//       </form>
//     );
//   };

//   export default TodoGenerator

import {useState} from "react";
import {nanoid} from "nanoid";
import {useDispatch} from "react-redux";
import {addTodo} from "./todoListSlice";
import {Button, Input} from "antd";

export const TodoGenerator = () => {
    const [todo, setTodo] = useState({id: 0,text: '',done:false})
    const dispatch = useDispatch()
    const handleInputChange = (e) => {
        setTodo({
            id: nanoid(),
            text: e.target.value,
            done: false
        })
    }
    const handleAdd = () => {
        dispatch(addTodo(todo))
        setTodo({})
    }
    return (
        <div className='todo-generator'>
            <Input value={todo.text} onChange={handleInputChange}/>
            <Button type={"primary"} onClick={handleAdd}>add</Button>
        </div>
    )
}
