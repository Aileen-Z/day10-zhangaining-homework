// import {Card} from 'antd'
// import {CheckOutlined, CloseOutlined} from "@ant-design/icons";
// import {useDispatch} from "react-redux";
// import {complete, deleteTodo} from "./todoListSlice";

// const delStyle = {textDecoration: 'line-through'}

// export const TodoItem = (props) => {
//     const {todo} = props
//     const dispatch = useDispatch()
//     const handleDelete = () => {
//         dispatch(deleteTodo(todo))
//     }

//     const handleComplete = () => {
//         dispatch(complete(todo))
//     }

//     return (
//         <div className="todo-item">
//             <Card className="todo-item-card" style={todo.done ? delStyle : ''}>
//                 {todo.text}
//             </Card>
//             <CheckOutlined onClick={handleComplete}/>
//             <CloseOutlined onClick={handleDelete}/>
//         </div>
//     )
// }
import './TodoList.css'
import {Card} from 'antd'
import {CheckOutlined, CloseOutlined} from "@ant-design/icons";
import {useDispatch} from "react-redux";
import {complete, deleteTodo} from "./todoListSlice";

const delStyle = {textDecoration: 'line-through'}

export const TodoItem = (props) => {
    const {todo} = props
    const dispatch = useDispatch()
    const handleDelete = () => {
        dispatch(deleteTodo(todo))
    }

    const handleComplete = () => {
        dispatch(complete(todo))
    }

    return (
        <div className="todo-item">
            <Card className="todo-item-card" style={todo.done ? delStyle : ''}>
                {todo.text}
            </Card>
            <CheckOutlined onClick={handleComplete}/>
            <CloseOutlined onClick={handleDelete}/>
        </div>
    )
}


